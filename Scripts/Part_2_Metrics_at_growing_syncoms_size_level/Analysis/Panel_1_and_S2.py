#!/usr/bin/env python

import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches # prints a warning : soon deprecated
import seaborn as sns

plt.style.use('ggplot')

path = '/path/to/project'

# ==== #
# Data #
# ==== #

df = pd.read_csv(os.path.join(path, 'Data/Syncoms_metrics/Data/data.csv'),
                 index_col=0,
                 header=0,
                 sep=',')

df.drop(['PhyloDist_Variance', 'Strains', 'Reactome', 'Core_Reactome'], axis=1, inplace=True)
df['SyncomSize'] = df['SyncomSize'].astype('category')
df = df.reindex(['Mean_PhyloDist','Mean_GenomeSize','SyncomSize', 'Scope', 'Core_Scope', 'Functional_Distance'], axis=1)

all_genomes_scope = 2383
all_genomes_core_scope = 263
all_genomes_phylodist = 1.2636063557858375
all_genomes_mean_genome_size = 5550824.067357513

# predictions
preds = pd.read_csv(os.path.join(path, 'Data/Syncoms_metrics/Data/predictions.csv'))

# ================ #
# Custom color map #
# ================ #

# Extended color range computed in a deprecated R script, copy-pasted here
legend_colors = ["#1B9E77", "#648549", "#AE6D1C", "#C8611F", "#A16864",
                 "#7A6FA9", "#9B58A5", "#C73C95", "#D8367D", "#A66753",
                 "#749829", "#89A716", "#BBA90B", "#E2A803", "#C9930D",
                 "#B07E18", "#97722D", "#7E6C49", "#666666"]
handles = [mpatches.Patch(color=legend_colors[i-2], label=str(i)) for i in range(2,21,1)]

handles = handles + [mpatches.Patch(color='black', label='Full community')]

# =========== #
# Main figure #
# =========== #
fig = plt.figure(constrained_layout=True, figsize=(10, 10))
gs = fig.add_gridspec(3, 2, height_ratios=[0.4,0.4,0.2])

ax_00 = fig.add_subplot(gs[0, 0])
ax_00.set_title('A', loc='center')
sns.scatterplot(x='Mean_PhyloDist', y='Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_00.plot(all_genomes_phylodist, all_genomes_scope, color='black', marker='o', markersize=12)

ax_01 = fig.add_subplot(gs[0, 1])
ax_01.set_title('B', loc='center')
sns.scatterplot(x='Mean_PhyloDist', y='Core_Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_01.plot(all_genomes_phylodist, all_genomes_core_scope, color='black', marker='o', markersize=12)

ax_10 = fig.add_subplot(gs[1, 0])
ax_10.set_title('C', loc='center')
sns.scatterplot(x='Mean_GenomeSize', y='Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_10.plot(all_genomes_mean_genome_size, all_genomes_scope, color='black', marker='o', markersize=12)

ax_11 = fig.add_subplot(gs[1, 1])
ax_11.set_title('D', loc='center')
sns.scatterplot(x='Mean_GenomeSize', y='Core_Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_11.plot(all_genomes_mean_genome_size, all_genomes_core_scope, color='black', marker='o', markersize=12)

plt.suptitle("Correlation between SynComs' PPM & CPPM, phylogenetic distance, and genome size")

ax_324 = fig.add_subplot(gs[2, 0:2])
ax_324.legend(handles=handles, loc='center', ncol=8, title='SynCom size')
ax_324.axis('off')

ax_00.set_xlabel('Mean Phylogenetic distance')
ax_00.set_ylabel('PPM')
ax_01.set_xlabel('Mean Phylogenetic distance')
ax_01.set_ylabel('CPPM')
ax_10.set_xlabel('Mean Genome size')
ax_10.set_ylabel('PPM')
ax_11.set_xlabel('Mean Genome size')
ax_11.set_ylabel('CPPM')

plt.savefig(os.path.join(path, 'Data/Syncoms_metrics/Figures/Main_plots/main_panel.png'), bbox_inches='tight')

# =================== #
# Supplemental figure #
# =================== #

fig = plt.figure(constrained_layout=True, figsize=(20, 15))
gs = fig.add_gridspec(4, 4)

# kde plots
ax_00 = fig.add_subplot(gs[0, 0])
ax_00.set_title('A1', loc='center')
sns.kdeplot(x='Mean_PhyloDist', y='Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_01 = fig.add_subplot(gs[0, 1])
ax_01.set_title('A2', loc='center')
sns.kdeplot(x='Mean_PhyloDist', y='Core_Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_10 = fig.add_subplot(gs[1, 0])
ax_10.set_title('A3', loc='center')
sns.kdeplot(x='Mean_GenomeSize', y='Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_11 = fig.add_subplot(gs[1, 1])
ax_11.set_title('A4', loc='center')
sns.kdeplot(x='Mean_GenomeSize', y='Core_Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)

# histograms
ax_20 = fig.add_subplot(gs[2, 0])
ax_20.set_title('C1', loc='center')
sns.kdeplot(x='Mean_PhyloDist', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_21 = fig.add_subplot(gs[2, 1])
ax_21.set_title('C2', loc='center')
sns.kdeplot(x='Mean_GenomeSize', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_30 = fig.add_subplot(gs[3, 0])
ax_30.set_title('C3', loc='center')
sns.kdeplot(x='Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_31 = fig.add_subplot(gs[3, 1])
ax_31.set_title('C4', loc='center')
sns.kdeplot(x='Core_Scope', data=df, hue='SyncomSize', palette=legend_colors, legend=False)

# boxplots
ax_02 = fig.add_subplot(gs[0, 2])
ax_02.set_title('B1', loc='center')
bplot1 = ax_02.boxplot([df[df.SyncomSize == x].Mean_PhyloDist for x in df.SyncomSize.unique()],
                           positions=range(2,21),
                           medianprops={'color': 'black'},
                           flierprops={'color': 'black'},
                           patch_artist=True)

ax_03 = fig.add_subplot(gs[0, 3])
ax_03.set_title('B2', loc='center')
bplot2 = ax_03.boxplot([df[df.SyncomSize == x].Mean_GenomeSize for x in df.SyncomSize.unique()],
                           positions=range(2,21),
                           medianprops={'color': 'black'},
                           flierprops={'color': 'black'},
                           patch_artist=True)

ax_12 = fig.add_subplot(gs[1, 2])
ax_12.set_title('B3', loc='center')
bplot3 = ax_12.boxplot([df[df.SyncomSize == x].Scope for x in df.SyncomSize.unique()],
                           positions=range(2,21),
                           medianprops={'color': 'black'},
                           flierprops={'color': 'black'},
                           patch_artist=True)

ax_13 = fig.add_subplot(gs[1, 3])
ax_13.set_title('B4', loc='center')
bplot4 = ax_13.boxplot([df[df.SyncomSize == x].Core_Scope for x in df.SyncomSize.unique()],
                           positions=range(2,21),
                           medianprops={'color': 'black'},
                           flierprops={'color': 'black'},
                           patch_artist=True)
for patch, color in zip(bplot1['boxes'], legend_colors):
    patch.set_facecolor(color)
for patch, color in zip(bplot2['boxes'], legend_colors):
    patch.set_facecolor(color)
for patch, color in zip(bplot3['boxes'], legend_colors):
    patch.set_facecolor(color)
for patch, color in zip(bplot4['boxes'], legend_colors):
    patch.set_facecolor(color)

ax_00.set_xlabel('Mean Phylogenetic distance')
ax_00.set_ylabel('PPM')
ax_01.set_xlabel('Mean Phylogenetic distance')
ax_01.set_ylabel('CPPM')
ax_10.set_xlabel('Mean Genome size')
ax_10.set_ylabel('PPM')
ax_11.set_xlabel('Mean Genome size')
ax_11.set_ylabel('CPPM')

ax_20.set_xlabel('Mean Phylogenetic distance')
ax_21.set_xlabel('Mean Genome size')
ax_30.set_xlabel('PPM')
ax_31.set_xlabel('CPPM')

ax_02.set_xlabel('SynCom size')
ax_03.set_xlabel('SynCom size')
ax_12.set_xlabel('SynCom size')
ax_13.set_xlabel('SynCom size')
ax_02.set_ylabel('Mean Phylogenetic distance')
ax_03.set_ylabel('Mean Genome size')
ax_12.set_ylabel('PPM')
ax_13.set_ylabel('CPPM')

# Independent variables autocorrelation check
ax_224 = fig.add_subplot(gs[2, 2:4])
ax_224.set_title('D', loc='center')
sns.scatterplot(x='Mean_PhyloDist', y='Mean_GenomeSize', data=df, hue='SyncomSize', palette=legend_colors, legend=False)
ax_224.set_xlabel('Mean Phylogenetic distance')
ax_224.set_ylabel('Mean Genome size')

# legend
ax_324 = fig.add_subplot(gs[3, 2:4])
ax_324.legend(handles=handles, loc='center', ncol=8, title='SynCom size')
ax_324.axis('off')

plt.savefig(os.path.join(path, 'Data/Syncoms_metrics/Figures/Supplemental/all_strains_scope_plots.png'), bbox_inches='tight')
