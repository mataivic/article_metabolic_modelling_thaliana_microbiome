#!/usr/bin/env python

import pickle
import os
from itertools import combinations
from random import sample
import pandas as pd
import numpy as np
from pprint import pprint

def get_strains_scope(community):

    p = os.path.join(PATH, 'Data/Core_data/AuReMe_reports_single_genomes')
    scopes = {}

    for strain in community:
        n = 'output_pathwaytools_{}/all_metabolites.csv'.format(strain)
        report = pd.read_csv(os.path.join(p, n), 
            index_col=0, 
            header=0,
            sep='\t')

        report = report[report['Produced (p), Consumed (c), Both (cp)'].isin(['p', 'cp'])]
        scopes[strain] = set(report.index)

    scope_union = len(set.union(*scopes.values()))
    scope_intersection = len(set.intersection(*scopes.values()))

    m = np.array([len(v) for v in scopes.values()])
    functional_distance = 1 - scope_intersection/np.mean(m)

    return scope_union, scope_intersection, functional_distance

"""
Compute metrics for a community
Mean phylogenetic distance and associated variance, reactions union, reactions intersection
"""
def community_metrics(community, reactions, distances_table, iso_summary):
    """
    - community : list with isolates names
    - reactions : dict isolate/reactions ids list
    - distances_table : 2D table of phylogenetic distances between isolates
    """
    # Compute reactions
    metabo = [reactions[iso] for iso in community]
    reactions_union = len(set.union(*metabo))
    reactions_intersection = len(set.intersection(*metabo))

    # Compute scopes
    scope_union, scope_intersection, functional_distance = get_strains_scope(community)

    # Get mean genome size
    sub_iso = iso_summary.loc[community, 'genome_size'].mean()

    # Compute phylogenetic distance
    pairs = list(combinations(community, 2))
    lp = len(pairs)
    dists = [distances_table.loc[pairs[i][0], pairs[i][1]] for i in range(0, lp)]
    dist_mean = np.mean(dists)
    dist_var = np.var(dists)

    return dist_mean, dist_var, sub_iso, reactions_union, reactions_intersection, scope_union, scope_intersection, functional_distance

PATH = '../../..'

# Extracted from AuReMe (except genome_size, extracted from annotation data)
isolates = pd.read_csv(os.path.join(PATH, 'Data/Core_data/isolates_rootGenomes_summary.csv'),
                                    index_col=0, 
                                    header=0, 
                                    sep=',')

# Extracted from SBMLs
reactions_path = 'Data/Core_data/Reactions_isolates_dict_from_SBML.pickle'
reactions = pickle.load(open(os.path.join(PATH, reactions_path), 'rb'))
# Extracted from phylogenetic tree
distances_table_path = 'Data/Core_data/phylogenetic_distances_pairs_isos.csv'
distances_table = pd.read_csv(os.path.join(PATH, distances_table_path),
                              header=0, 
                              index_col=0)

# Computation on random SynComs
comms_sizes = range(2,21,1)
sample_size = 500

# keys : commumity sizes ; values : list of lists (one list per metric)
idt = 1 # identifier
names = ['ID', 'Mean_PhyloDist', 'PhyloDist_Variance', 'Mean_GenomeSize', 'Reactome', 'Core_Reactome', 'Strains']

df_list = []

for size in comms_sizes:

    print('Computing size {} ...'.format(size))
    metrics = {'ID': ['']*sample_size,
               'Mean_PhyloDist': np.zeros(sample_size),
               'PhyloDist_Variance': np.zeros(sample_size),
               'Mean_GenomeSize' : np.zeros(sample_size),
               'Reactome': np.zeros(sample_size),
               'Core_Reactome': np.zeros(sample_size),
               'Scope': np.zeros(sample_size),
               'Core_Scope': np.zeros(sample_size),
               'Functional_Distance' : np.zeros(sample_size),
               'SyncomSize' : [size]*sample_size,
               'Strains': ['']*sample_size
               }

    # metrics calculations
    for n in range(0, sample_size):
        r_comm = sample(list(isolates.index), size)        
        m, v, s, u_r, i_r, u_s, i_s, f = community_metrics(r_comm, reactions, distances_table, isolates)
        metrics['ID'][n] = idt
        metrics['Mean_PhyloDist'][n] = m
        metrics['PhyloDist_Variance'][n] = v
        metrics['Mean_GenomeSize'][n] = s
        metrics['Reactome'][n] = u_r
        metrics['Core_Reactome'][n] = i_r
        metrics['Scope'][n] = u_s
        metrics['Core_Scope'][n] = i_s
        metrics['Functional_Distance'][n] = f
        metrics['Strains'][n] = '-'.join(r_comm)
        idt += 1

    df = pd.DataFrame.from_dict(metrics)
    df = df.set_index('ID')
    df_list.append(df)
    df.to_csv(os.path.join(PATH, 'Data/Part_2_Metrics_at_growing_syncoms_size_level/Splitted_by_size', str(size)+'.csv'))

data = pd.concat(df_list)
data.to_csv(os.path.join(PATH, 'Data/Part_2_Metrics_at_growing_syncoms_size_level', 'data.csv'))

# Full community

m, v, s, u_r, i_r, u_s, i_s, f = community_metrics(list(isolates.index), reactions, distances_table, isolates)

print('Mean_PhyloDist: {}'.format(m))
print('PhyloDist_Variance: {}'.format(v))
print('Mean_GenomeSize: {}'.format(s))
print('Reactome: {}'.format(u_r))
print('Core_Reactome: {}'.format(i_r))
print('Scope: {}'.format(u_s))
print('Core_Scope: {}'.format(i_s))
print('Functional_Distance: {}'.format(f))
