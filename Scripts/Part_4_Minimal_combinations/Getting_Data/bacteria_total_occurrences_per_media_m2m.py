#!/usr/bin/env python

import os
import json
from pprint import pprint
import pandas as pd

PATH = '../../../Data/Part_4_Minimal_Combinations'

# ================ #
# Annotation based #
# ================ #

strains = os.listdir(os.path.join(PATH, 'SBML_symlinks'))
strains = [strain.rstrip('.sbml') for strain in strains]

# ----------- #
# All targets #
# ----------- #

mincoms = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/all_targets'))
if "deprecated" in mincoms:
    mincoms.remove("deprecated")
columns = [mincom.replace('_all_targets.json', '').replace('seeds_', '') for mincom in mincoms]

# Occurrences of bacteria in solutions, per media
df = pd.DataFrame(0, index=strains, columns=columns)

for mincom in mincoms:

    with open(os.path.join(PATH, 'Outputs_M2M/Annotation_based/all_targets/{}/json/all_targets.json'.format(mincom))) as f:
        data = json.load(f)

    solutions = data['enum_bacteria']
    column = mincom.replace('_all_targets.json', '').replace('seeds_', '')

    for solution in solutions.values():
        for strain in solution:
            df.loc[strain, column] += 1

df.to_csv(os.path.join(PATH, 'dfs/strains_occurrences/Annotation_based/strains_total_occurrences_per_media_all_solutions_all_targets_m2m_annotation_based.csv'))

# ----------- #
# Amino-acids #
# ----------- #

mincoms = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/amino_acids_targets'))
columns = [mincom.replace('_targets_amino_acids.json', '').replace('seeds_', '') for mincom in mincoms]

# df contains aas, vitamins, hormones at the same time
df = pd.DataFrame(0, index=strains, columns=columns)

for mincom in mincoms:

    with open(os.path.join(PATH, 'Outputs_M2M/Annotation_based/amino_acids_targets/{}/json/amino_acids_targets.json'.format(mincom))) as f:
        data = json.load(f)

    solutions = data['enum_bacteria']
    column = mincom.replace('_targets_amino_acids.json', '').replace('seeds_', '')

    for solution in solutions.values():
        for strain in solution:
            df.loc[strain, column] += 1

df.to_csv(os.path.join(PATH, 'dfs/strains_occurrences/Annotation_based/strains_total_occurrences_per_media_all_solutions_amino_acids_m2m_annotation_based.csv'))


# -------- #
# Vitamins #
# -------- #

mincoms = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/vitamins_targets'))
columns = [mincom.replace('_targets_vitamins.json', '').replace('seeds_', '') for mincom in mincoms]

# df contains aas, vitamins, hormones at the same time
df = pd.DataFrame(0, index=strains, columns=columns)

for mincom in mincoms:

    with open(os.path.join(PATH, 'Outputs_M2M/Annotation_based/vitamins_targets/{}/json/vitamins_targets.json'.format(mincom))) as f:
        data = json.load(f)

    solutions = data['enum_bacteria']
    column = mincom.replace('_targets_vitamins.json', '').replace('seeds_', '')

    for solution in solutions.values():
        for strain in solution:
            df.loc[strain, column] += 1

df.to_csv(os.path.join(PATH, 'dfs/strains_occurrences/Annotation_based/strains_total_occurrences_per_media_all_solutions_vitamins_m2m_annotation_based.csv'))

# ------------- #
# phyto_hormones #
# ------------- #

mincoms = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/phyto_hormones_targets'))
columns = [mincom.replace('_targets_phyto_hormones.json', '').replace('seeds_', '') for mincom in mincoms]

# df contains aas, vitamins, hormones at the same time
df = pd.DataFrame(0, index=strains, columns=columns)

for mincom in mincoms:

    with open(os.path.join(PATH, 'Outputs_M2M/Annotation_based/phyto_hormones_targets/{}/json/phyto_hormones_targets.json'.format(mincom))) as f:
        data = json.load(f)

    solutions = data['enum_bacteria']
    column = mincom.replace('_targets_phyto_hormones.json', '').replace('seeds_', '')

    for solution in solutions.values():
        for strain in solution:
            df.loc[strain, column] += 1

df.to_csv(os.path.join(PATH, 'dfs/strains_occurrences/Annotation_based/strains_total_occurrences_per_media_all_solutions_phyto_hormones_m2m_annotation_based.csv'))
