#!/bin/bash

path=../../../

# ========================== #
# All targets, mineral media #
# ========================== #

echo '--- all_targets_mineral_media ---'

media=($(ls $path/Data/Core_data/ASP/ASP_seeds/Mineral))

for m in "${media[@]}"
do
    n="${m%.*}"_all_targets
    echo $n
    mkdir $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/all_targets/$n

    m2m_analysis enum \
                -n $path/Data/Part_4_Minimal_Combinations/SBML_symlinks \
                -s $path/Data/Core_data/ASP/ASP_seeds/Mineral/$m \
                -t $path/Data/Core_data/ASP/ASP_targets/all_targets.sbml \
                -o $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/all_targets/$n > /dev/null 2>&1
done

# ========================== #
# Amino acids, mineral media #
# ========================== #

echo '--- Amino_acids_mineral_media ---'

media=($(ls $path/Data/Data/Core_data/ASP/ASP_seeds/Mineral))

for m in "${media[@]}"
do
    n="${m%.*}"_amino_acids_targets
    echo $n
    mkdir $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/amino_acids_targets/$n

    m2m_analysis enum \
                -n $path/Data/Part_4_Minimal_Combinations/SBML_symlinks \
                -s $path/Data/Core_data/ASP/ASP_seeds/Mineral/$m \
                -t $path/Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/amino_acids_targets.sbml \
                -o $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/amino_acids_targets/$n > /dev/null 2>&1
done

# ========================== #
# Amino acids, organic media #
# ========================== #

echo '--- Amino_acids_organic_media ---'

media=($(ls $path/Data/Core_data/ASP/ASP_seeds/Organic))

for m in "${media[@]}"
do
    n="${m%.*}"_amino_acids_targets
    echo $n
    mkdir $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/amino_acids_targets/$n

    m2m_analysis enum \
                -n $path/Data/Part_4_Minimal_Combinations/SBML_symlinks \
                -s $path/Data/Core_data/ASP/ASP_seeds/Organic/$m \
                -t $path/Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/amino_acids_targets.sbml \
                -o $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/amino_acids_targets/$n > /dev/null 2>&1
done

# ======================= #
# Vitamins, mineral media #
# ======================= #

echo '--- Vitamins_mineral_media ---'

media=($(ls $path/Data/Core_data/ASP/ASP_seeds/Mineral))

for m in "${media[@]}"
do
    n="${m%.*}"_vitamins_targets
    echo $n
    mkdir $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/vitamins_targets/$n

    m2m_analysis enum \
                -n $path/Data/Part_4_Minimal_Combinations/SBML_symlinks \
                -s $path/Data/Core_data/ASP/ASP_seeds/Mineral/$m \
                -t $path/Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/vitamins_targets.sbml \
                -o $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/vitamins_targets/$n > /dev/null 2>&1
done

# ============================= #
# Phyto hormones, mineral media #
# ============================= #

echo '--- Phyto_hormones_mineral_media ---'

media=($(ls $path/Data/Core_data/ASP/ASP_seeds/Mineral))

for m in "${media[@]}"
do
    n="${m%.*}"_phyto_hormones_targets
    echo $n
    mkdir $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets/$n

    m2m_analysis enum \
                -n $path/Data/Part_4_Minimal_Combinations/SBML_symlinks \
                -s $path/Data/Core_data/ASP/ASP_seeds/Mineral/$m \
                -t $path/Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/phyto_hormones_targets.sbml \
                -o $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets/$n > /dev/null 2>&1
done

# ============================= #
# Phyto hormones, organic media #
# ============================= #

echo '--- Phyto_hormones_organic_media ----'

media=($(ls $path/Data/Core_data/ASP/ASP_seeds/Organic))

for m in "${media[@]}"
do
    n="${m%.*}"_phyto_hormones_targets
    echo $n
    mkdir $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets/$n

    m2m_analysis enum \
                -n $path/Data/Part_4_Minimal_Combinations/SBML_symlinks \
                -s $path/Data/Core_data/ASP/ASP_seeds/Organic/$m \
                -t $path/Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/phyto_hormones_targets.sbml \
                -o $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets/$n > /dev/null 2>&1
done

# ============================== #
# Phyto hormones, organic+ media #
# ============================== #

echo '--- Phyto_hormones_organic+_media ---'

media=($(ls $path/Data/Core_data/ASP/ASP_seeds/Organic_mixture))

for m in "${media[@]}"
do
    n="${m%.*}"_phyto_hormones_targets
    echo $n
    mkdir $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets/$n

    m2m_analysis enum \
                -n $path/Data/Part_4_Minimal_Combinations/SBML_symlinks \
                -s $path/Data/Core_data/ASP/ASP_seeds/Organic_mixture/$m \
                -t $path/Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/phyto_hormones_targets.sbml \
                -o $path/Data/Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets/$n > /dev/null 2>&1
done

