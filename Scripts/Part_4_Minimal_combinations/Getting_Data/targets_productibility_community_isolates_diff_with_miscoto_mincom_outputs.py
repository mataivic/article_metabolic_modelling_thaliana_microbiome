#!/usr/bin/env python

import os
import json
from pprint import pprint
import pandas as pd

"""
Using Miscoto Mincom outputs of isolates :

1.Gets targets producible by single_strains according to media
2.Gets targets producible by community according to media
3.Gets targets productibility difference between community and single isolates
"""

"""
DF with
 - media as index
 - 3 columns:
    - Targets producible by the community
    - Targets still unproductible
    - Targets producibles by single strains
    - Difference between column 1 and 3
"""

PATH = '../../../Data'

# ======================================== #
# Needed for fullcom targets prod capacity #
# ======================================== #

# Split per media according to targets
media_all_and_vits = ["seeds_M63.json", "seeds_M63_ARE.json",
                      "seeds_M9.json", "seeds_M9_ARE.json",
                      "seeds_mineral_medium.json", "seeds_mineral_medium_ARE.json",
                      "seeds_hydrogen_oxydizing.json", "seeds_hydrogen_oxydizing_ARE.json",
                      "seeds_MBM.json", "seeds_MBM_ARE.json"]

media_amino_acids = media_all_and_vits + ["seeds_basal.json", "seeds_basal_ARE.json",
                                          "seeds_phb_pyruvate.json", "seeds_phb_pyruvate_ARE.json",
                                          "seeds_MMJS.json", "seeds_MMJS_ARE.json"]

media_phyto = media_amino_acids + ["seeds_LB_lennox_enriched.json", "seeds_LB_lennox_enriched_ARE.json"]

# targets lists to filter json after reading
aminoacids = set(["M_ARG_c", "M_L__45__ASPARTATE_c", "M_ASN_c", "M_CYS_c", "M_GLN_c",
                 "M_GLY_c", "M_HIS_c", "M_ILE_c", "M_LEU_c", "M_LYS_c", "M_MET_c",
                 "M_PHE_c", "M_PRO_c", "M_THR_c", "M_TRP_c", "M_TYR_c", "M_VAL_c"])

vitamins = set(["M_THIAMINE__45__PYROPHOSPHATE_c", "M_RIBOFLAVIN_c", "M_NIACINE_c",
                "M_PANTOTHENATE_c", "M_PYRIDOXINE_c", "M_BIOTIN_c", "M_THF_c",
                "M_ADENOSYLCOBALAMIN_c"])

phytohormones = set(["M_CPD__45__734_c", "M_INDOLE_ACETATE_AUXIN_c", "M_CPD__45__110_c",
                     "M_CPD__45__693_c", "M_ETHYLENE__45__CMPD_c"])

alls = set.union(aminoacids, vitamins, phytohormones)

# ================ #
# Annotation based #
# ================ #

# =========== #
# All targets #
# =========== #

# Get miscoto mincom data ---------------------------------------------------
mincoms = os.listdir(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/all_targets'))

rownames = [mincom.replace('_all_targets', '').replace('seeds_', '') for mincom in mincoms]
colnames = ['mincom_producible_targets', # targets producible by minimal combinations (miscoto_mincom based)
            'mincom_unproducible_targets', # targets still unproducible by minimal combinations (miscoto_mincom based)
            'miscoto_single_strains_producible_targets', # targets producible by single strains (miscoto_scope based)
            'miscoto_single_strains_unproducible_targets', # targets punroducible by single strains (miscoto_scope based)
            'fullcom_producible_targets', # targets producible by the whole community as a meta-organsim (miscoto_scope based)
            'fullcom_unproducible_targets'] # targets unproducible by the whole community as a meta-organsim (miscoto_scope based)

df_all = pd.DataFrame(0, index=rownames, columns=colnames)

# Miscoto mincom
for mincom in mincoms:

    with open(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/all_targets/{}/json/all_targets.json'.format(mincom))) as f:
        data = json.load(f)

    mincom_producible_targets = set(data['producible'])
    mincom_unproducible_targets = set(data['still_unprod'])

    row = mincom.replace('_all_targets', '').replace('seeds_', '')

    df_all.loc[row] = [len(mincom_producible_targets),
                       len(mincom_unproducible_targets),
                       0,0,0,0]

# Miscoto fullcom
for medium in media_all_and_vits:
    with open(os.path.join(PATH, 'Core_data/Community_scope/Annotation_based', medium), 'r') as f:
        d = json.load(f)

    prod_targets = set(d['com_prodtargets'])
    unprod_targets = set(d['com_unprodtargets'])
    newly_prod_targets = prod_targets.difference(unprod_targets)
    df_all.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_producible_targets'] = len(prod_targets.intersection(alls))
    df_all.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_unproducible_targets'] = len(unprod_targets)

# Single strains
media = [medium.replace('.json', '') for medium in media_all_and_vits]

for medium in media:
    prod_targets = set([])
    isos = os.listdir(os.path.join(PATH, 'Core_data/Miscoto_scopes/Annotation_based', medium))
    for iso in isos:
        with open(os.path.join(PATH, 'Core_data/Miscoto_scopes/Annotation_based', medium, iso), 'r') as f:
            d = json.load(f)
        prod_targets_iso = set(d['com_prodtargets'])
        prod_targets = prod_targets.union(prod_targets_iso)
    unprod_targets = alls.difference(prod_targets)
    df_all.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_producible_targets'] = len(prod_targets)
    df_all.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_unproducible_targets'] = len(unprod_targets)

df_all.to_csv(os.path.join(PATH, 'Part_4_Minimal_Combinations/dfs/targets_producibility_comparisons/all_targets_producibility_community_strains_difference_miscoto_mincom_outputs_annotation_based.csv'))

# =================== #
# Amino acids targets #
# =================== #

# Get miscoto mincom data ---------------------------------------------------
mincoms = os.listdir(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/amino_acids_targets'))

rownames = [mincom.replace('_amino_acids_targets', '').replace('seeds_', '') for mincom in mincoms]

df_aas = pd.DataFrame(0, index=rownames, columns=colnames)

# Compute differences between isolated strains end mincoms
for mincom in mincoms:

    with open(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/amino_acids_targets/{}/json/amino_acids_targets.json'.format(mincom))) as f:
        data = json.load(f)

    mincom_producible_targets = set(data['producible'])
    mincom_unproducible_targets = set(data['still_unprod'])

    row = mincom.replace('_amino_acids_targets', '').replace('seeds_', '')

    df_aas.loc[row] = [len(mincom_producible_targets),
                       len(mincom_unproducible_targets),
                       0,0,0,0]

# Add Miscoto fullcom
for medium in media_amino_acids:
    with open(os.path.join(PATH, 'Core_data/Community_scope/Annotation_based', medium), 'r') as f:
        d = json.load(f)

    prod_targets = set(d['com_prodtargets'])
    unprod_targets = set(d['com_unprodtargets'])
    df_aas.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_producible_targets'] = len(prod_targets.intersection(aminoacids))
    df_aas.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_unproducible_targets'] = len(unprod_targets.intersection(aminoacids))

# Single strains - !! All targets -> filter at some point
media = [medium.replace('.json', '') for medium in media_amino_acids]

for medium in media:
    prod_targets = set([])
    isos = os.listdir(os.path.join(PATH, 'Core_data/Miscoto_scopes/Annotation_based', medium))
    for iso in isos:
        with open(os.path.join(PATH, 'Core_data/Miscoto_scopes/Annotation_based', medium, iso), 'r') as f:
            d = json.load(f)
        prod_targets_iso = set(d['com_prodtargets'])
        prod_targets = prod_targets.union(prod_targets_iso)
    prod_targets = prod_targets.intersection(aminoacids)
    unprod_targets = aminoacids.difference(prod_targets)
    df_aas.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_producible_targets'] = len(prod_targets)
    df_aas.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_unproducible_targets'] = len(unprod_targets)

    if medium == 'seeds_phb_pyruvate':
        print(prod_targets)
        print(unprod_targets)

df_aas.to_csv(os.path.join(PATH, 'Part_4_Minimal_Combinations/dfs/targets_producibility_comparisons/amino_acids_targets_producibility_community_strains_difference_miscoto_mincom_outputs_annotation_based.csv'))

# ================ #
# Vitamins targets #
# ================ #

# Get miscoto mincom data ---------------------------------------------------
mincoms = os.listdir(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/vitamins_targets'))

rownames = [mincom.replace('_vitamins_targets', '').replace('seeds_', '') for mincom in mincoms]

df_vits = pd.DataFrame(0, index=rownames, columns=colnames)

# Compute differences between isolated strains end mincoms
for mincom in mincoms:

    with open(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/vitamins_targets/{}/json/vitamins_targets.json'.format(mincom))) as f:
        data = json.load(f)

    mincom_producible_targets = set(data['producible'])
    mincom_unproducible_targets = set(data['still_unprod'])

    row = mincom.replace('_vitamins_targets', '').replace('seeds_', '')

    df_vits.loc[row] = [len(mincom_producible_targets),
                        len(mincom_unproducible_targets),
                        0,0,0,0]

# Add Miscoto fullcom
for medium in media_all_and_vits:
    with open(os.path.join(PATH, 'Core_data/Community_scope/Annotation_based', medium), 'r') as f:
        d = json.load(f)

    prod_targets = set(d['com_prodtargets'])
    unprod_targets = set(d['com_unprodtargets'])
    df_vits.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_producible_targets'] = len(prod_targets.intersection(vitamins))
    df_vits.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_unproducible_targets'] = len(unprod_targets.intersection(vitamins))

# Single strains
media = [medium.replace('.json', '') for medium in media_all_and_vits]

for medium in media:
    prod_targets = set([])
    isos = os.listdir(os.path.join(PATH, 'Core_data/Miscoto_scopes/Annotation_based', medium))
    for iso in isos:
        with open(os.path.join(PATH, 'Core_data/Miscoto_scopes/Annotation_based', medium, iso), 'r') as f:
            d = json.load(f)
        prod_targets_iso = set(d['com_prodtargets'])
        prod_targets = prod_targets.union(prod_targets_iso)
    prod_targets = prod_targets.intersection(vitamins)
    unprod_targets = vitamins.difference(prod_targets)
    df_vits.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_producible_targets'] = len(prod_targets)
    df_vits.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_unproducible_targets'] = len(unprod_targets)

df_vits.to_csv(os.path.join(PATH, 'Part_4_Minimal_Combinations/dfs/targets_producibility_comparisons/vitamins_targets_producibility_community_strains_difference_miscoto_mincom_outputs_annotation_based.csv'))

# ====================== #
# Phyto hormones targets #
# ====================== #

# Get miscoto mincom data ---------------------------------------------------
mincoms = os.listdir(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets'))

rownames = [mincom.replace('_phyto_hormones_targets', '').replace('seeds_', '') for mincom in mincoms]

df_phyto = pd.DataFrame(0, index=rownames, columns=colnames)

# Compute differences between isolated strains end mincoms
for mincom in mincoms:

    with open(os.path.join(PATH, 'Part_4_Minimal_Combinations/Outputs_M2M/Annotation_based/phyto_hormones_targets/{}/json/phyto_hormones_targets.json'.format(mincom))) as f:
        data = json.load(f)

    mincom_producible_targets = set(data['producible'])
    mincom_unproducible_targets = set(data['still_unprod'])

    row = mincom.replace('_phyto_hormones_targets', '').replace('seeds_', '')

    df_phyto.loc[row] = [len(mincom_producible_targets),
                        len(mincom_unproducible_targets),
                        0,0,0,0]

for medium in media_phyto:
    with open(os.path.join(PATH, 'Core_data/Community_scope/Annotation_based', medium), 'r') as f:
        d = json.load(f)

    prod_targets = set(d['com_prodtargets'])
    unprod_targets = set(d['com_unprodtargets'])
    df_phyto.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_producible_targets'] = len(prod_targets.intersection(phytohormones))
    df_phyto.loc[medium.replace('.json', '').replace('seeds_', ''), 'fullcom_unproducible_targets'] = len(unprod_targets.intersection(phytohormones))

# Single strains
media = [medium.replace('.json', '') for medium in media_phyto]

for medium in media:
    prod_targets = set([])
    isos = os.listdir(os.path.join(PATH, '.Core_data/Miscoto_scopes/Annotation_based', medium))
    for iso in isos:
        with open(os.path.join(PATH, 'Core_data/Miscoto_scopes/Annotation_based', medium, iso), 'r') as f:
            d = json.load(f)
        prod_targets_iso = set(d['com_prodtargets'])
        prod_targets = prod_targets.union(prod_targets_iso)
    prod_targets = prod_targets.intersection(phytohormones)
    unprod_targets = phytohormones.difference(prod_targets)
    df_phyto.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_producible_targets'] = len(prod_targets)
    df_phyto.loc[medium.replace('seeds_', ''), 'miscoto_single_strains_unproducible_targets'] = len(unprod_targets)

df_phyto.to_csv(os.path.join(PATH, 'Part_4_Minimal_Combinations/dfs/targets_producibility_comparisons/phyto_hormones_targets_producibility_community_strains_difference_miscoto_mincom_outputs_annotation_based.csv'))
