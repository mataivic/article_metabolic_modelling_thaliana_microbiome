#!/usr/bin/env python

import os
import pandas as pd
import numpy as np
import json
from pprint import pprint

"""
Build a mincom summary table of solutions number and size for each growth media
"""

def make_summary(media_list, media_folder):

    rows = {media.replace('seeds_', '').replace('_'+media_folder, ''):0 for media in media_list}

    cols = ['n_sol', 'n_bact_in_sol', 'n_bact',
            'n_sol_prod_targets', 'n_sol_new_targets', 'n_sol_unprod_targets',
            'n_com_prod_targets', 'n_com_new_targets', 'n_com_unprod_targets']

    dic = {col:rows for col in cols}
    df = pd.DataFrame.from_dict(dic)

    for media in media_list:

        media_fmt = media.replace('seeds_', '').replace('_'+media_folder, '')

        with open(os.path.join(PATH, 'Outputs_M2M/Annotation_based', media_folder, media, 'json', media_folder+'.json'), 'r') as f:
            d = json.load(f)

        df.loc[media_fmt, 'n_sol'] = len(d['enum_bacteria']) # nb of solutions
        df.loc[media_fmt, 'n_bact_in_sol'] = len(d['enum_bacteria']["1"]) # nb strains in a solution (first solution listed)
        df.loc[media_fmt, 'n_bact'] = len(d['union_bacteria'])
        df.loc[media_fmt, 'n_sol_prod_targets'] = len(d['one_model']['producible_target'])
        df.loc[media_fmt, 'n_sol_new_targets'] = len(d['one_model']['newly_producible_target'])
        df.loc[media_fmt, 'n_sol_unprod_targets'] = len(d['one_model']['unproducible_target'])
        df.loc[media_fmt, 'n_com_prod_targets'] = len(d['producible'])
        df.loc[media_fmt, 'n_com_new_targets'] = len(d['newly_prod'])
        df.loc[media_fmt, 'n_com_unprod_targets'] = len(d['still_unprod'])

    return df

PATH = '../../../Data/Part_4_Minimal_Combinations'

# ================ #
# Annotation based #
# ================ #

# Media folders
media_all_targets = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/all_targets'))
media_amino_acids = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/amino_acids_targets'))
media_vitamins = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/vitamins_targets'))
media_phyto_hormones = os.listdir(os.path.join(PATH, 'Outputs_M2M/Annotation_based/phyto_hormones_targets'))

df = make_summary(media_all_targets, 'all_targets')
df.sort_index(axis=0, inplace=True)
df.to_csv(os.path.join(PATH, 'dfs/summaries/Annotation_based/mincoms_all_targets_summary.csv'))

df = make_summary(media_amino_acids, 'amino_acids_targets')
df.sort_index(axis=0, inplace=True)
df.to_csv(os.path.join(PATH, 'dfs/summaries/Annotation_based/mincoms_amino_acids_targets_summary.csv'))

df = make_summary(media_vitamins, 'vitamins_targets')
df.sort_index(axis=0, inplace=True)
df.to_csv(os.path.join(PATH, 'dfs/summaries/Annotation_based/mincoms_vitamins_targets_summary.csv'))

df = make_summary(media_phyto_hormones, 'phyto_hormones_targets')
df.sort_index(axis=0, inplace=True)
df.to_csv(os.path.join(PATH, 'dfs/summaries/Annotation_based/mincoms_phyto_hormones_targets_summary.csv'))

