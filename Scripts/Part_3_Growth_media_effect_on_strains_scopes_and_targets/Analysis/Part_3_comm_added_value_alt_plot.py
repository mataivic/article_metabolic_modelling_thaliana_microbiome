#!/usr/bin/env python

import matplotlib.pyplot as plt

plt.style.use('ggplot')

labels = ["M63","M63_ARE","M9", "M9_ARE","mineral_medium","mineral_medium_ARE", 
          "hydrogen_oxydizing","hydrogen_oxydizing_ARE", "MBM", "MBM_ARE","basal",
          "basal_ARE", "phb_pyruvate", "phb_pyruvate_ARE", "MMJS", "MMJS_ARE",
          "LB_lennox_enriched", "LB_lennox_enriched_ARE"]

# hard-coded here (copy_pasted from results DataFrame)
single_strains = [208, 384, 208, 384, 210, 386, 211, 387, 209, 385, 288, 411, 
                  371, 420, 357, 411, 433, 460]
comm_added_value = [283, 98, 278, 93, 283, 98, 296, 111, 296, 111, 225, 93, 149,
                    91, 157, 95, 146, 113]
percent_increase = [136, 26, 134, 24, 135, 25, 140, 29, 142, 29, 78, 23, 40, 22, 44, 23, 34, 25]

width = 0.75 # the width of the bars: can also be len(x) sequence

fig, ax = plt.subplots(figsize=(5,1.75))

ax.bar(labels, single_strains, width,
       label='Producible by single strains', color='cornflowerblue')
ax.bar(labels, comm_added_value, width,
       bottom=single_strains,
       label='Community added value',
       color='salmon')

ax.set_xlabel('Growth media (nutritional constraint)', color='black', fontsize=8)
ax.set_xticklabels(labels, rotation=45, ha='right')
ax.set_ylabel('PPM', color='black', fontsize=8)
ax.set_title('C', loc='left')
ax.legend(fontsize=8, loc=('lower center'))

# labels format
weights = ['normal', 'bold', 'normal', 'bold', 'normal', 'bold', 'normal', 'bold',
           'normal', 'bold', 'normal', 'bold', 'normal', 'bold', 'normal', 'bold',
           'normal', 'bold']
colors = ['black', 'black','black','black','black','black','black','black','black','black',
          'red', 'red','red','red','red','red','red','red',]

i=0
for label in (ax.get_xticklabels()):
    font_props = {'family': 'normal',
                  'weight': weights[i]}
    label.set_font(font_props)
    label.set_color(colors[i])
    label.set_fontsize(6)
    i+=1

plt.savefig('../../../Figures/comm_added_val.png', bbox_inches='tight')

