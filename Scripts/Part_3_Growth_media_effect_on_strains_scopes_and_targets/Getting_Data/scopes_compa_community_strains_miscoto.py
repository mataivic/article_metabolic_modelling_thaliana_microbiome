#!/usr/bin/env python

import os
import json
from pprint import pprint
import pandas as pd#!/usr/bin/env python

import os
import json
from pprint import pprint
import pandas as pd

"""
return a dataframe with community scope under growth media. Splits unions of
isolates scopes and community added value
"""

PATH = '/path/to/Data/'

# ================ #
# Annotation based #
# ================ #

print('Annotation based\n================')

# Get miscoto mincom data ---------------------------------------------------
media = os.listdir(os.path.join(PATH, 'ISOLATES/Community_scope/Annotation_based/'))
if "deprecated" in media:
    media.remove("deprecated")
if "bact_dir" in media:
    media.remove("bact_dir")

rownames = [medium.replace('.json', '').replace('seeds_', '') for medium in media]
colnames = ['community_scope', 
            'community_newly_producible_scope',
            'single_strains_scope']

df = pd.DataFrame(0, index=rownames, columns=colnames)

# Compute differences between isolated strains and mincoms
for medium in media:
    print(medium)

    # Community scope
    with open(os.path.join(PATH, 'ISOLATES/Community_scope/Annotation_based/{}'.format(medium))) as f:
        data = json.load(f)
    community_scope = set(data['com_scope'])    

    # Strain-by-strain scope
    single_strains_scope = set([])    
    isos = os.listdir(os.path.join(PATH, 'ISOLATES/Menetools_outputs/Miscoto_scopes/Annotation_based/{}'.format(medium.replace('.json', ''))))
    for iso in isos:
        with open(os.path.join(PATH, 'ISOLATES/Menetools_outputs/Miscoto_scopes/Annotation_based/{}/{}'.format(medium.replace('.json', ''), iso))) as f:
            data = json.load(f)
        single_strains_scope.update(data['com_scope'])    

    print(len(community_scope), len(single_strains_scope))
    community_newly_producible_scope = community_scope - single_strains_scope

    # Fill row
    row = medium.replace('.json', '').replace('seeds_', '')
    df.loc[row] = [len(community_scope), 
                   len(community_newly_producible_scope),
                   len(single_strains_scope)]    

df.to_csv(os.path.join(PATH, 'High_Level_Analysis/dfs/comparison_scopes_community_strains_miscoto_scopes_annotation_based.csv'))
