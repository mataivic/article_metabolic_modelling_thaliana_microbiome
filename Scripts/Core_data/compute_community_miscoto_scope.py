#!/usr/bin/env python

# source activate env_menetools

import os
from miscoto import run_scopes

PATH = '../..'
bact_path_annot = 'Data/Core_data/ASP/all_bacteria_sbml'
out_path_annot = 'Data/Core_data/Community_scope/Annotation_based/'

# 1/ Mineral media =============================================================
print('Mineral media ...')
SEEDS_PATH = 'Data/Core_data/ASP/ASP_seeds/Mineral'
MEDIA = os.listdir(os.path.join(PATH, SEEDS_PATH))
TARGETS_PATH = 'Data/Core_data/ASP/ASP_targets/all_targets.sbml'

print('Annotation based ...')
for medium in MEDIA:

    print(medium)

    run_scopes(targets_file=os.path.join(PATH, TARGETS_PATH),
               seeds_file=os.path.join(PATH, SEEDS_PATH, medium),
               bacteria_dir=os.path.join(PATH, bact_path_annot),
               output_json=os.path.join(PATH, out_path_annot, medium.replace('.sbml', '.json')))

# 2/ Organic media =============================================================
print('Organic media ...')
SEEDS_PATH = 'Data/Core_data/ASP/ASP_seeds/Organic'
MEDIA = os.listdir(os.path.join(PATH, SEEDS_PATH))
TARGETS_PATH = 'Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/amino_acids_and_phytohormones_targets.sbml'

print('Annotation based ...')
for medium in MEDIA:

    print(medium)

    run_scopes(targets_file=os.path.join(PATH, TARGETS_PATH),
               seeds_file=os.path.join(PATH, SEEDS_PATH, medium),
               bacteria_dir=os.path.join(PATH, bact_path_annot),
               output_json=os.path.join(PATH, out_path_annot, medium.replace('.sbml', '.json')))

# 3/ Organic mixture media =====================================================
print('Organic mixture media ...')
SEEDS_PATH = 'Data/Core_data/ASP/ASP_seeds/Organic_mixture'
MEDIA = os.listdir(os.path.join(PATH, SEEDS_PATH))
TARGETS_PATH = 'Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/phyto_hormones_targets.sbml'

print('Annotation based ...')
for medium in MEDIA:

    print(medium)

    run_scopes(targets_file=os.path.join(PATH, TARGETS_PATH),
               seeds_file=os.path.join(PATH, SEEDS_PATH, medium),
               bacteria_dir=os.path.join(PATH, bact_path_annot),
               output_json=os.path.join(PATH, out_path_annot, medium.replace('.sbml', '.json')))
