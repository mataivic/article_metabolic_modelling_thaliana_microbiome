#!/usr/bin/env python

import os
import pandas as pd
from ete3 import Tree

path='../..'

# Genomes
genomes = pd.read_csv(os.path.join(path, 'Data/Core_data/Genomes_metrics_summaries/isolates_rootGenomes_summary.csv'),
                      index_col=0,
                      header=0,
                      sep=',')

# phylogenetic tree
with open(os.path.join(path, 'Data/Core_data/Genomes_metrics_summaries/spp_tree.newick'), 'r') as f:
    tree_file = f.read().strip('\n')    
phylo_tree = Tree(tree_file)

# Dataframe for phylogenetic distance between genomes
nodes_distance = pd.DataFrame(0, index=genomes.index, columns=genomes.index)

for node in nodes_distance.index:
    for node2 in nodes_distance.index:
        if node != node2:
            d = phylo_tree.get_distance(node, node2)
            nodes_distance.loc[node, node2] = d
            nodes_distance.loc[node2, node] = d
            
nodes_distance.to_csv(os.path.join(path, 'Data/Core_data/Genomes_metrics_summaries/phylogenetic_distances_pairs_isos.csv'))
