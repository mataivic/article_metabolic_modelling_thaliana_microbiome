#!/usr/bin/env python

import os
import pandas as pd

PATH = '../..'

strains = os.listdir(os.path.join(PATH, 'Data/Core_data/Genomes_metrics_summaries/AuReMe_reports_single_genomes'))

# Get draft scope
draft = pd.read_csv(os.path.join(PATH, 'Data/Core_data/Genomes_metrics_summaries/AuReMe_report_metagenome/all_metabolites.csv'),
                    index_col=0,
                    header=0,
                    sep='\t')

strains_fmt = [strain.split('_')[-1] for strain in strains] # format genomes names
df = pd.DataFrame(0, index=strains_fmt, columns=draft.index)

for strain in strains:
    strain_fmt = strain.split('_')[-1]
    print(strain_fmt)

    metabolites = pd.read_csv(os.path.join(PATH, 'Data/Core_data/Genomes_metrics_summaries/AuReMe_reports_single_genomes', strain, 'all_metabolites.csv'),
                              index_col=0,
                              header=0,
                              sep='\t')

    metabolites = metabolites.index

    for metabolite in df.columns:
        if metabolite in metabolites:
            df.loc[strain_fmt, metabolite] = 1

df.to_csv(os.path.join(PATH, 'Data/Core_data/Genomes_metrics_summaries/full_scopes_per_iso.csv'))
