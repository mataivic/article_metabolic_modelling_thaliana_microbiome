#!/usr/bin/env python

import pandas as pd
import os
from lxml import etree

"""
Parse SBML metabolic networks to get absence/presence of all reactions in all genomes

SBML networks were obtained from Padmet networks (with a padmet.utils scripts)
Padmet networks were obtained with AuReMe
AuReMe inputs were Annotated genomes obtained by pathway tools
"""

# Main path and strains list
PATH = '../..'
ISOS = os.listdir(os.path.join(PATH, 'Data/Core_data/SBML_metabolic_networks'))

"""
SBML parsing to get all reactions IDs
return : a set of reactions IDs
"""
def get_SBML_reactions(path):
    with open(path, 'r') as f:
        sbml = f.read()

    sbml = sbml.replace(' xmlns="http://www.sbml.org/sbml/level2"', '')
    sbml = bytes(bytearray(sbml, encoding='utf-8'))
    sbml = etree.fromstring(sbml)
    reactions = set(reaction.attrib['id'] for reaction in sbml.xpath('/sbml/model/listOfReactions/reaction'))

    return reactions

def build_matrix(isos, end_path, outfile_name):
    # Main loop 1 - Get all reactions list per strain
    reactions_per_iso = {}
    for iso in isos:
        rxns = get_SBML_reactions(os.path.join(PATH, end_path, iso, iso+'.sbml'))
        reactions_per_iso[iso] = rxns

    # Main loop 2 -
    all_reactions = list(set.union(*reactions_per_iso.values()))
    tmp = {True:1, False:0}
    reactions_per_iso_completed = {}
    for iso in isos:
        abspres = [rxn in reactions_per_iso[iso] for rxn in all_reactions]
        rxns = [tmp[ap] for ap in abspres]
        reactions_per_iso_completed[iso] = rxns

    # Output df
    df = pd.DataFrame.from_dict(reactions_per_iso_completed, orient='index', columns=all_reactions)
    df.to_csv(os.path.join(PATH, outfile_name))

def main():

    build_matrix(ISOS,
                 'Data/Core_data/SBML_metabolic_networks',
                 'Data/Core_data/Genomes_metrics_summaries/Reactions_isolates_annotation_based_binary.csv')

if __name__ == "__main__":
    main()
