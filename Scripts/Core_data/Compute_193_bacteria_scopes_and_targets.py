#!/usr/bin/env python

# conda activate envt_menetools_recent

import os
from miscoto import run_scopes

# Global variables

PATH = '../..'

ALL_TARGETS_PATH = 'Data/Core_data/ASP/ASP_targets/all_targets.sbml'
ALMOST_ALL_TARGETS_PATH = 'Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/amino_acids_and_phytohormones_targets.sbml'
PHYTO_TARGETS_PATH = 'Data/Core_data/ASP/ASP_targets/Targets_separated_for_miscoto_mincom/phyto_hormones_targets.sbml'

# Compute scopes for all strains and all media
def compute_all(bact_path, seeds_path, targets_path, media, out_path_miscoto):

    isolates = os.listdir(os.path.join(PATH, bact_path))

    i=1
    for iso in isolates:
        print('  Strain nb {} ({}) ----------------------------'.format(i, iso))
        i+=1

        for medium in media:
            print('    '+medium)

            # Community scope used on only one strain. Exclude seeds, includes targets
            print('      miscoto scope ...')
            run_scopes(targets_file=os.path.join(PATH, targets_path),
                       seeds_file=os.path.join(PATH, seeds_path, medium),
                       bacteria_dir=os.path.join(PATH, bact_path, iso),
                       output_json=os.path.join(PATH, out_path_miscoto, medium.replace('.sbml', ''), '{}.json'.format(iso)))

def main():

    print('# ======== Annotation-based ======== #')

    bact_path = 'Data/Core_data/SBML_metabolic_networks'
    out_path_miscoto = 'Data/Core_data/Miscoto_scopes/Annotation_based'

    # 1/ Mineral media
    seeds_path = 'Data/Core_data/ASP/ASP_seeds/Mineral'
    media = os.listdir(os.path.join(PATH, seeds_path))

    for medium in media:
        tmp = medium.replace('.sbml', '')
        if not os.path.exists(os.path.join(PATH, out_path_miscoto, tmp)):
            os.mkdir(os.path.join(PATH, out_path_miscoto, tmp))

    compute_all(bact_path, seeds_path, ALL_TARGETS_PATH, media, out_path_miscoto)

    # 2/ Organic media
    seeds_path = 'ASP/ASP_seeds/Organic'
    media = os.listdir(os.path.join(PATH, seeds_path))

    for medium in media:
        tmp = medium.replace('.sbml', '')
        if not os.path.exists(os.path.join(PATH, out_path_miscoto, tmp)):
            os.mkdir(os.path.join(PATH, out_path_miscoto, tmp))
        if not os.path.exists(os.path.join(PATH, out_path_targets, tmp)):

    compute_all(bact_path, seeds_path, ALMOST_ALL_TARGETS_PATH, media, out_path_miscoto)

    # 3/ Organic mixture media
    seeds_path = 'ASP/ASP_seeds/Organic_mixture'
    media = os.listdir(os.path.join(PATH, seeds_path))

    for medium in media:
        tmp = medium.replace('.sbml', '')
        if not os.path.exists(os.path.join(PATH, out_path_miscoto, tmp)):
            os.mkdir(os.path.join(PATH, out_path_miscoto, tmp))

    compute_all(bact_path, seeds_path, PHYTO_TARGETS_PATH, media, out_path_miscoto)

if __name__ == "__main__":
    main()
