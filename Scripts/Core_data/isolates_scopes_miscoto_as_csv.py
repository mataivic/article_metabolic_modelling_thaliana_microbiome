#!/usr/bin/env python

import os
import json
import csv
import pandas as pd

path1 = '../..'
path3 = 'Data/Core_data/Genomes_metrics_summaries/AuReMe_report_metagenome'

# Full scopes (no constraints) -------------------------------------------------

isolates = os.listdir(os.path.join(path1, path3))
scopes_full = {}

for iso in isolates:
    file = os.path.join(path1, path3, iso, 'all_metabolites.csv')
    metabolites = pd.read_csv(file, header=0, index_col=0, sep='\t')
    iso_name = iso.split('_')[-1]
    scopes_full[iso_name] = len(metabolites)

# Constrained scopes -----------------------------------------------------------

path2 = 'Data/Core_data/Miscoto_scopes'
out = "isolates_scopes_miscoto_annotation_based.csv"

folders = os.listdir(os.path.join(path1, path2))
if out in folders:
    folders.remove(out)

# list of isolates for df row names and len
isolates = os.listdir(os.path.join(path1, path2, 'seeds_M9'))
isolates_index = [iso.rstrip('.json') for iso in isolates]

df = pd.DataFrame(0, index=isolates_index, columns=folders)

for folder in folders:
    for isolate in isolates:
        with open(os.path.join(path1, path2, folder, isolate), 'r') as f:
            scope = json.load(f)
            len_scope = len(scope['com_scope'])
            df.loc[isolate.rstrip('.json'), folder] = len_scope

colnames = {c:c[6:] for c in df.columns}
df.rename(columns=colnames, inplace=True)
df['full_scopes'] = pd.Series(scopes_full)
df.to_csv(os.path.join(path1, path2, out))
